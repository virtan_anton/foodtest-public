package com.example.akadem.foodtest;


import android.util.Log;
import android.util.Xml;

import com.example.akadem.foodtest.pojo.Offer;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UfaFarforXmlParser {

    final String LOG_TAG = "MY_TAG";
    private static final String ns = null;

    public List<Offer> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            parser.nextTag();
            return readShop(parser);
        } finally {
            in.close();
        }
    }

    private List<Offer> readShop(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, ns, "shop");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("offers")) {
                return readOffers(parser);
            } else {
                skip(parser);
            }
        }
        return null;
    }

    private List<Offer> readOffers(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<Offer> offers = new ArrayList<>();

        parser.require(XmlPullParser.START_TAG, ns, "offers");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("offer")){
                offers.add(readOffer(parser));
            } else {
                skip(parser);
            }
        }
        return offers;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private Offer readOffer(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "offer");

        String url = null;
        String name = null;
        String price = null;
        String description = null;
        String picture = null;
        int categoryId = 0;
        Map<String, String> params = new HashMap<>();

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG){
                continue;
            }

            String tagName = parser.getName();
            if (tagName.equals("url")) {
                url = readUrl(parser);
            } else if (tagName.equals("name")) {
                name = readName(parser);
            } else if (tagName.equals("price")) {
                price = readPrice(parser);
            } else if (tagName.equals("description")) {
                description = readDescription(parser);
            } else if (tagName.equals("picture")) {
                picture = readPictureUrl(parser);
            } else if (tagName.equals("categoryId")) {
                categoryId = readCategoryId(parser);
            } else if (tagName.equals("param")) {
                String[] param = readParam(parser);
                params.put(param[0],param[1]);
            } else skip(parser);
        }

        return new Offer(url, name, price, description, picture, categoryId, params);
    }

    private String readUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "url");
        String url = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "url");
        return url;
    }

    private String readName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "name");
        String name = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "name");
        return name;
    }

    private String readPrice(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "price");
        String price = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "price");
        return price;
    }

    private String readDescription(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "description");
        String description = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "description");
        return description;
    }

    private String readPictureUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "picture");
        String url = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "picture");
        return url;
    }

    private int readCategoryId(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "categoryId");
        int id = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, "categoryId");
        return id;
    }

    private String[] readParam(XmlPullParser parser) throws IOException, XmlPullParserException {
        String name = "";
        String value = "";
        parser.require(XmlPullParser.START_TAG, ns, "param");
        String attributeName = parser.getAttributeValue(null, "name");
        if (attributeName != null){
            name = attributeName;
            value = readText(parser);
        }
        return new String[]{name, value};
    }


    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private int readInt(XmlPullParser parser) throws IOException, XmlPullParserException {
        int result = 0;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Integer.valueOf(parser.getText());
            parser.nextTag();
        }
        return result;
    }



}
