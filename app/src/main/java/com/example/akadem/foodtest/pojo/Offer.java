package com.example.akadem.foodtest.pojo;

import java.util.Map;

public class Offer {
    public final String url;
    public final String name;
    public final String price;
    public final String description;
    public final String pictureUrl;
    public final int categoryId;
    public final Map<String, String> params;

    public Offer(String url, String name, String price, String description, String pictureUrl, int categoryId, Map<String, String> params) {
        this.url = url;
        this.name = name;
        this.price = price;
        this.description = description;
        this.pictureUrl = pictureUrl;
        this.categoryId = categoryId;
        this.params = params;
    }
}
