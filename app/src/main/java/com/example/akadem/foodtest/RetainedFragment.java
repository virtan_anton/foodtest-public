package com.example.akadem.foodtest;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseArray;

import com.example.akadem.foodtest.pojo.Offer;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RetainedFragment extends Fragment {

    private List<Offer> offers = null;
    private int downloadXmlResultCode = 0;
    private SparseArray<ArrayList<Offer>> offersByCategory = null;
    private DownloadXmlData downloadTask = new DownloadXmlData();

    final String LOG_TAG = "MY_TAG";

    public interface XmlDownloadListener {
        void onDownloadCompleted(int downloadXmlResultCode);
    }

    XmlDownloadListener downloadListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            downloadListener = (XmlDownloadListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement XmlDownloadListener");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void downloadXmlData() {
        if (downloadXmlResultCode != 1) {
            downloadTask.execute("http://ufa.farfor.ru/getyml/?key=ukAXxeJYZN");
        }
    }


    private void parseOffersByCategory(List<Offer> offers) {
        offersByCategory = new SparseArray<>();
        for (Categories category : Categories.values()) {
            offersByCategory.put(category.getCategoryId(), getOfferListWithId(offers, category.getCategoryId()));
        }
    }

    private ArrayList<Offer> getOfferListWithId(List<Offer> allOffersList, int categoryId) {
        ArrayList<Offer> list = new ArrayList<>();
        for (int iOffer = 0; iOffer < allOffersList.size(); iOffer++) {
            Offer offer = allOffersList.get(iOffer);
            if (offer.categoryId == categoryId) {
                list.add(offer);
            }
        }
        return list;
    }

    public ArrayList<Offer> getOffers(int categoryId) {
        Log.d(LOG_TAG, "getOffers(" + categoryId + ")");
        if (downloadXmlResultCode == 1) {
            Log.d(LOG_TAG, "getOffers(" + categoryId + "), offers number: " + offersByCategory.get(categoryId).size());
            return offersByCategory.get(categoryId);
        } else return null;
    }




    private class DownloadXmlData extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            try {
                int result = loadXmlFromNetwork(params[0]);
                parseOffersByCategory(offers);
                return result;
            } catch (IOException e) {
                System.err.println(e);
                return -1;
            } catch (XmlPullParserException e) {
                System.err.println(e);
                return -2;
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            downloadXmlResultCode = integer;
            downloadListener.onDownloadCompleted(integer);
        }

        private int loadXmlFromNetwork(String urlString) throws IOException, XmlPullParserException {
            InputStream stream = null;
            UfaFarforXmlParser ufaFarforXmlParser = new UfaFarforXmlParser();

            try {
                stream = downloadUrl(urlString);
                offers = ufaFarforXmlParser.parse(stream);
                Log.d(LOG_TAG, "loadXmlFromNetwork(), offers.size(): " + offers.size());
            } finally {
                if (stream != null) {
                    stream.close();
                }
            }

            return 1;
        }

        private InputStream downloadUrl(String urlString) throws IOException {
            Log.d(LOG_TAG, "downloadUrl(), urlString: " + urlString);
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            return conn.getInputStream();
        }

    }

}
