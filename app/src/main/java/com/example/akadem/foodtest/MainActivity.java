package com.example.akadem.foodtest;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.akadem.foodtest.pojo.Offer;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity
        implements
        NavigationView.OnNavigationItemSelectedListener,
        TabFragmentPrototype.FragmentCooperation,
        RetainedFragment.XmlDownloadListener{

    final String RETAINED_FRAGMENT_TAG = "RETAINED_FRAGMENT_TAG";
    FragmentManager fManager;
    RetainedFragment retainedFragment;

    boolean isDataDownloaded = false;

    final String LOG_TAG = "MY_TAG";


    ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null){
            isDataDownloaded = savedInstanceState.getBoolean("isDataDownloaded");
        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ViewPager viewPager = (ViewPager) findViewById(R.id.tabanim_viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        fManager = getSupportFragmentManager();
        if (fManager.findFragmentByTag(RETAINED_FRAGMENT_TAG) == null) {
            retainedFragment = new RetainedFragment();
            fManager.beginTransaction().add(retainedFragment, RETAINED_FRAGMENT_TAG).commit();
            fManager.executePendingTransactions();
        }
        retainedFragment = (RetainedFragment) fManager.findFragmentByTag(RETAINED_FRAGMENT_TAG);

        if (retainedFragment == null) {
            Log.d(LOG_TAG, "retainedFragment == null");
        }

    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (Categories category : Categories.values()) {
            adapter.addFrag(category, getString(category.getNameStringRes()));
        }
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        retainedFragment.downloadXmlData();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isDataDownloaded",isDataDownloaded);
    }

    private void updateViewData() {
        SparseArray<Fragment> registeredFragments = adapter.getRegisteredFragments();
        for (int i = 0; i < registeredFragments.size(); i++) {
            int key = registeredFragments.keyAt(i);
            TabFragmentPrototype tab = (TabFragmentPrototype) registeredFragments.get(key);
            tab.fillData();
            Log.d(LOG_TAG, "updateViewData(), tab("+key+") fillData()");
        }
    }
    // ===FRAGMENTS INTERFACES IMPLEMENTATION===
    @Override
    public ArrayList<Offer> getOffers(int categoryId) {
        return retainedFragment.getOffers(categoryId);
    }

    @Override
    public boolean checkData() {
        return isDataDownloaded;
    }

    @Override
    public void onDownloadCompleted(int downloadXmlResultCode) {
        Log.d(LOG_TAG,"onDownloadCompleted(), downloadXmlResultCode = "+ downloadXmlResultCode);
        if (downloadXmlResultCode == 1) {
            updateViewData();
            isDataDownloaded = true;
        } else if (downloadXmlResultCode == -1){
            // error IOException
        } else if (downloadXmlResultCode == -2){
            // error XmlParserException
        }
    }
    //==========================================


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
