package com.example.akadem.foodtest;

import java.io.Serializable;

/**
 * Created by Akadem on 30.11.2015.
 */
public enum Categories implements Serializable {
    category_20(R.string.id_20_category, 20),
    category_18(R.string.id_18_category, 18),
    category_23(R.string.id_23_category, 23),
    category_8(R.string.id_8_category, 8),
    category_7(R.string.id_7_category, 7),
    category_6(R.string.id_6_category, 6),
    category_10(R.string.id_10_category, 10),
    category_2(R.string.id_2_category, 2),
    category_5(R.string.id_5_category, 5),
    category_24(R.string.id_24_category, 24),
    category_1(R.string.id_1_category, 1),
    category_9(R.string.id_9_category, 9),
    category_3(R.string.id_3_category, 3);

    private final int nameStringRes;
    private final int categoryId;

    Categories(int nameStringRes, int categoryId) {
        this.nameStringRes = nameStringRes;
        this.categoryId = categoryId;
    }

    public int getNameStringRes() {
        return nameStringRes;
    }

    public int getCategoryId() {
        return categoryId;
    }
}
