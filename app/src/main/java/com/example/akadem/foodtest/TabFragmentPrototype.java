package com.example.akadem.foodtest;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.akadem.foodtest.pojo.Offer;

import java.util.ArrayList;


public class TabFragmentPrototype extends Fragment {

    public interface FragmentCooperation {
        ArrayList<Offer> getOffers(int categoryId);
        boolean checkData();
    }

    FragmentCooperation cooperator;

    private View v;
    private Categories category;
    private RelativeLayout containerLayout;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);


        try {
            cooperator = (FragmentCooperation) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement FragmentCooperation");
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null){
            category = (Categories) savedInstanceState.getSerializable("category");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.tab_prototype, null);
        containerLayout = (RelativeLayout) v.findViewById(R.id.container);
        if (cooperator.checkData()) {
            fillData();
        }
        return v;

    }

    public void fillData() {
        containerLayout.removeAllViews();
        LayoutInflater inflater = getLayoutInflater(null);
        View v = inflater.inflate(R.layout.tab_recycler_view, null);

        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        recyclerView.setAdapter(new RecycleViewAdapter(this.getContext(), cooperator.getOffers(category.getCategoryId())));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), 2));
        containerLayout.addView(recyclerView);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("category",category);
    }

    public void setCategory(Categories category) {
        this.category = category;
    }
}
