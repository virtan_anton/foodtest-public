package com.example.akadem.foodtest;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.akadem.foodtest.pojo.Offer;

import java.util.ArrayList;
import java.util.Map;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder> {

    private ArrayList<Offer> offers;
    private Context context;

    final String LOG_TAG = "MY_TAG";

    public RecycleViewAdapter(Context context, ArrayList<Offer> offers) {
        this.offers = offers;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_card_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Offer offer = offers.get(position);
        String offerWeight = offer.params.get("Вес");

        holder.name.setText(offer.name);
        holder.weight.setText(offerWeight);
        holder.price.setText(offer.price);
        Glide.with(context)
                .load(offer.pictureUrl)
                .into(holder.img);
        if (position == 1){
            Log.d(LOG_TAG,"onBindViewHolder(), offer.params.size(): "+ offer.params.size() );
            for (Map.Entry<String,String> entry : offer.params.entrySet()){
                Log.d(LOG_TAG, entry.getKey()+": "+entry.getValue());
            }


            Log.d(LOG_TAG, "onBindViewHolder(), offer name: "+offer.name+
                    ". offer weight: "+offerWeight+
                    ". offer price: "+offer.price+
                    ". offer pictureUrl: "+offer.pictureUrl);
        }

    }


    @Override
    public int getItemCount() {
        return offers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView weight;
        TextView price;
        ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.offer_name);
            weight = (TextView) itemView.findViewById(R.id.offer_weight);
            price = (TextView) itemView.findViewById(R.id.offer_price);
            img = (ImageView) itemView.findViewById(R.id.offer_img);
        }
    }

}
